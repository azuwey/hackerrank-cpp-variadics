/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/02/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/cpp-variadics/problem
 **/
#include <iostream>
#include <bitset>
#include <vector>

using namespace std;

template <bool... D>
int reversed_binary_value() {
  vector<bool> arr = {D...};
  const int s = sizeof...(D);
  bitset<s> bs;

  for (size_t i = 0; i < s; ++i) {
    bs[i] = arr[i];
  }
  return static_cast<int>(bs.to_ulong());
};

template <int n, bool... digits>
struct CheckValues {
  static void check(int x, int y) {
    CheckValues<n - 1, 0, digits...>::check(x, y);
    CheckValues<n - 1, 1, digits...>::check(x, y);
  }
};

template <bool... digits>
struct CheckValues<0, digits...> {
  static void check(int x, int y) {
    int z = reversed_binary_value<digits...>();
    std::cout << (z + 64 * y == x);
  }
};

int main() {
  int t;
  std::cin >> t;

  for (int i = 0; i != t; ++i) {
    int x, y;
    cin >> x >> y;
    CheckValues<6>::check(x, y);
    cout << "\n";
  }
}
